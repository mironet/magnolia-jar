# Get magnolia webapp
FROM alpine:3.10 AS builder

ARG ANSIBLE_VERSION=2.8.5

ENV BUILD_PACKAGES \
  bash \
  curl \
  tar \
  openssh-client \
  sshpass \
  git \
  python3 \
  py3-dateutil \
  py3-httplib2 \
  py3-lxml \
  py3-jinja2 \
  py3-paramiko \
  py3-pip \
  py3-yaml \
  ca-certificates

RUN set -x && \
    \
    echo "==> Adding build-dependencies..."  && \
    apk --update add --virtual build-dependencies \
      gcc \
      musl-dev \
      libffi-dev \
      openssl-dev \
      python3-dev && \
    \
    echo "==> Upgrading apk and system..."  && \
    apk update && apk upgrade && \
    \
    echo "==> Adding Python runtime..."  && \
    apk add --no-cache ${BUILD_PACKAGES} && \
    pip3 install --upgrade pip && \
    pip3 install python-keyczar docker-py && \
    \
    echo "==> Installing Ansible..."  && \
    pip install ansible==${ANSIBLE_VERSION} && \
    \
    echo "==> Cleaning up..."  && \
    apk del build-dependencies && \
    rm -rf /var/cache/apk/* && \
    \
    echo "==> Adding hosts for convenience..."  && \
    mkdir -p /etc/ansible /ansible && \
    echo "[local]" >> /etc/ansible/hosts && \
    echo "localhost ansible_connection=local" >> /etc/ansible/hosts

ENV ANSIBLE_GATHERING false
ENV ANSIBLE_HOST_KEY_CHECKING true
ENV ANSIBLE_RETRY_FILES_ENABLED false
ENV ANSIBLE_ROLES_PATH /ansible/playbooks/roles
ENV ANSIBLE_SSH_PIPELINING True
ENV PYTHONPATH /ansible/lib
ENV PATH /ansible/bin:$PATH
ENV ANSIBLE_LIBRARY /ansible/library

ARG MVN_GROUP_ID
ARG MVN_ARTIFACT_ID
ARG MVN_VERSION
ARG MVN_DEST
ARG MVN_REPO_URL

WORKDIR /ansible/playbooks

COPY playbook.yml playbook.yml

RUN mkdir /jars && \
    ansible-playbook -vvvv playbook.yml

FROM busybox:latest AS final

COPY --from=builder /jars/ /jars/
COPY init.sh /init.sh

RUN chmod +x /init.sh && \
    chmod 644 /jars/* && \
    ls -Fahl /jars && \
    ls -Fahl /