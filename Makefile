JAR_NAME=magnolia-cloud-bootstrapper-1.0.2.jar
USER_ID=$(shell id -u)
PKG_LIST=`go list ./... | grep -v /vendor/`

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

build-postgres: ## Build docker images.
	docker build . -t magnolia-jar:postgres-42.2.8 \
		--build-arg MVN_GROUP_ID=org.postgresql \
		--build-arg MVN_ARTIFACT_ID=postgresql \
		--build-arg MVN_VERSION=42.2.8 \
		--build-arg MVN_DEST=/jars/postgresql.jar

build-jmxexporter: ## Build docker images.
	docker build . -t magnolia-jar:jmxexporter-0.13.0 \
		--build-arg MVN_GROUP_ID=io.prometheus.jmx \
		--build-arg MVN_ARTIFACT_ID=jmx_prometheus_javaagent \
		--build-arg MVN_VERSION=0.13.0 \
		--build-arg MVN_DEST=/jars/jmx_prometheus_javaagent-0.13.0.jar

build-cloud-bootstrap: ## Build docker images.
	docker build . -t magnolia-jar:magnolia-cloud-bootstrapper-1.0 \
		--build-arg MVN_GROUP_ID=info.magnolia.cloud \
		--build-arg MVN_ARTIFACT_ID=magnolia-cloud-bootstrapper \
		--build-arg MVN_VERSION=1.0 \
		--build-arg MVN_REPO_URL=https://nexus.magnolia-cms.com/content/groups/public \
		--build-arg MVN_DEST=/jars/magnolia-cloud-bootstrapper-1.0.jar

build-magnolia-content-indexer: ## Build docker images.
	docker build . -f external/Dockerfile --target final-local -t magnolia-jar:magnolia-content-indexer-1.0 \
		--build-arg SOURCE_FILE=./external/jars/magnolia-content-indexer-6.0.1.jar \
		--build-arg JAR_NAME=magnolia-content-indexer-6.0.1.jar

build-docker-go: ## Build magnolia-jar-go docker image.
	docker build --build-arg APP_VERSION=$$(git describe --always) -t gitlab.com/mironet/magnolia-jar-go -f go/Dockerfile ./go

test-jar-go: build-docker-go ## Test magnolia-jar-go docker image.
	docker run -v $$(pwd)/go/test:/test:rw -e JAR_LIST_0="$(JAR_NAME):/test" --user $(USER_ID):$(USER_ID) --rm gitlab.com/mironet/magnolia-jar-go

test: ## Execute all tests.
	cd go && go test -race $(PKG_LIST) -count 1 -v && cd ..