package main

import (
	"encoding/json"
	"fmt"
	"io"
	"math"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"
)

type JarList []Jar

type Jar struct {
	Name          string `json:"name"`
	DestDir       string `json:"destDir"`
	OverwriteName string `json:"overwriteName,omitempty"`
}

func jarList(path string) (JarList, error) {
	if path == "" {
		fmt.Printf("no jar list path specified, reading jar list from env vars\n")
		return jarListFromEnv()
	}

	file, err := os.Open(path)
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Printf("specified jar list file not found, reading jar list from env vars\n")
			return jarListFromEnv()
		}
		return nil, fmt.Errorf("could not open file: %w", err)
	}
	bytes, err := io.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("could not read file: %w", err)
	}
	out := JarList{}
	if err := json.Unmarshal(bytes, &out); err != nil {
		return nil, fmt.Errorf("could not unmarshal bytes: %w", err)
	}
	return out, nil
}

func jarListFromEnv() (JarList, error) {
	// Read the jar list from environment variables.
	out := JarList{}
	for i := 0; ; i++ {
		entry := os.Getenv(fmt.Sprintf("JAR_LIST_%d", i))
		if entry == "" {
			break
		}
		parts := strings.Split(entry, ":")
		switch len(parts) {
		case 2:
			out = append(out, Jar{
				Name:    parts[0],
				DestDir: parts[1],
			})
			fmt.Printf("jar %s must be loaded to destination %s\n", parts[0], parts[1])
		case 3:
			out = append(out, Jar{
				Name:          parts[0],
				DestDir:       parts[1],
				OverwriteName: parts[2],
			})
			fmt.Printf("jar %s must be loaded to destination %s with name %s\n", parts[0], parts[1], parts[2])
		default:
			return nil, fmt.Errorf("invalid envvar jar list entry %d: %s", i, entry)
		}
	}
	fmt.Printf("successfully read jar list of lenght %d from env vars\n", len(out))
	return out, nil
}

func (j Jar) load(sourcePath string) error {
	sourceFile, err := os.Open(path.Join(sourcePath, j.Name))
	if err != nil {
		return fmt.Errorf("could not open source file: %w", err)
	}
	defer sourceFile.Close()

	name := path.Join(j.DestDir, j.Name)
	if j.OverwriteName != "" {
		name = path.Join(j.DestDir, j.OverwriteName)
	}
	destFile, err := os.Create(name)
	if err != nil {
		return fmt.Errorf("could not create dest file: %w", err)
	}
	defer destFile.Close()

	if _, err := io.Copy(destFile, sourceFile); err != nil {
		return fmt.Errorf("could not copy file: %w", err)
	}

	return nil
}

type version struct {
	major  int
	minor  int
	bugfix int
	suffix string
}

// compare returns -1 if self is less than other, 0 if self is equal to other, and 1 if self is greater than other.
func (self version) compare(other version) (int, error) {
	if self.major == -1 && other.major != -1 ||
		self.major != -1 && other.major == -1 ||
		self.minor == -1 && other.minor != -1 ||
		self.minor != -1 && other.minor == -1 ||
		self.bugfix == -1 && other.bugfix != -1 ||
		self.bugfix != -1 && other.bugfix == -1 {
		return math.MinInt, fmt.Errorf("could not compare versions %v and %v", self, other)
	}
	switch {
	case self.major > other.major ||
		self.major == other.major && self.minor > other.minor ||
		self.major == other.major && self.minor == other.minor && self.bugfix > other.bugfix ||
		self.major == other.major && self.minor == other.minor && self.bugfix == other.bugfix && self.suffix != "" && other.suffix == "":
		return 1, nil
	case self.major < other.major ||
		self.major == other.major && self.minor < other.minor ||
		self.major == other.major && self.minor == other.minor && self.bugfix < other.bugfix ||
		self.major == other.major && self.minor == other.minor && self.bugfix == other.bugfix && self.suffix == "" && other.suffix != "":
		return -1, nil
	case self.major == other.major &&
		self.minor == other.minor &&
		self.bugfix == other.bugfix &&
		self.suffix == other.suffix:
		return 0, nil
	default:
		return math.MinInt, fmt.Errorf("could not compare versions %v and %v", self, other)
	}
}

var (
	jarVersionPartRegex = regexp.MustCompile(`-(\d.*?)\.jar$`)
	numberPrefixRegex   = regexp.MustCompile(`^(0$|[1-9]\d*)`)
)

func fetchVersion(jarName string) (version, error) {
	versionPart := jarVersionPartRegex.FindString(jarName)
	versionPart = strings.TrimPrefix(versionPart, "-")
	versionPart = strings.TrimSuffix(versionPart, ".jar")
	parts := strings.Split(versionPart, ".")
	out := version{
		major:  -1,
		minor:  -1,
		bugfix: -1,
		suffix: "",
	}

	for i, part := range parts {
		var intPtr *int
		switch i {
		case 0:
			intPtr = &out.major
		case 1:
			intPtr = &out.minor
		case 2:
			intPtr = &out.bugfix
		default:
			out.suffix = "." + strings.Join(parts[i:], ".")
			continue
		}
		number := numberPrefixRegex.FindString(part)
		if part != number {
			if number == "" {
				out.suffix = "."
			}
			out.suffix = out.suffix + strings.TrimPrefix(part, number)
		}
		if number != "" {
			convInt, err := strconv.Atoi(number)
			if err != nil {
				return out, fmt.Errorf("could not convert part %q (#%d) to int: %w", part, i, err)
			}
			*intPtr = convInt
		}
		if part != number {
			break
		}
	}
	return out, nil
}

func artifactID(jarName string) (string, error) {
	versionPart := jarVersionPartRegex.FindString(jarName)
	if versionPart == "" {
		return strings.TrimSuffix(jarName, ".jar"), nil
	}
	return strings.TrimSuffix(jarName, versionPart), nil
}
