package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
)

type ArtifactDescriptorList []ArtifactDescriptor

type ArtifactDescriptor struct {
	GroupID    string `json:"groupID"`
	ArtifactID string `json:"artifactID"`
	Version    string `json:"version"`
	RepoURL    string `json:"repoURL"`
	DestName   string `json:"destName,omitempty"`
	DestDir    string `json:"destDir"`
}

func artifactDescriptorList(path string) (ArtifactDescriptorList, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("could not open file: %w", err)
	}
	bytes, err := io.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("could not read file: %w", err)
	}
	out := ArtifactDescriptorList{}
	if err := json.Unmarshal(bytes, &out); err != nil {
		return nil, fmt.Errorf("could not unmarshal bytes: %w", err)
	}
	return out, nil
}

func (a ArtifactDescriptor) download(targetPath string) error {
	url, err := url.Parse(a.RepoURL)
	if err != nil {
		return fmt.Errorf("could not parse repo url %s: %w", a.RepoURL, err)
	}
	sourceName := strings.Join([]string{a.ArtifactID, a.Version}, "-") + ".jar"
	url.Path = path.Join(url.Path, strings.ReplaceAll(a.GroupID, ".", "/"), a.ArtifactID, a.Version, sourceName)

	resp, err := http.Get(url.String())
	if err != nil {
		return fmt.Errorf("could not get url: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		bytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("could not read response body of response (status %d) for GET %s: %w", resp.StatusCode, url, err)
		}
		return fmt.Errorf("unexpected response (status %d) for GET %s: %s", resp.StatusCode, url, string(bytes))
	}

	out, err := os.OpenFile(targetPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return fmt.Errorf("could not open file %s: %w", a.DestName, err)
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return fmt.Errorf("could not copy response body to file %s: %w", a.DestName, err)
	}

	return nil
}
