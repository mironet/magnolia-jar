
# magnolia-jar-go

## New usage of `jars` block in magnolia-helm chart

```yaml
jars:
  - name: load-additional-jars
    repository: registry.gitlab.com/mironet/magnolia-jar/magnolia-jar-go
    tag: v0.1.0
    env:
      - name: JAR_LIST_0
        value: postgresql-42.7.3.jar:/app/magnolia/WEB-INF/lib:postgresql.jar
      - name: JAR_LIST_1
        value: jmx_prometheus_javaagent-1.0.1.jar:/extraLibs/:jmx_prometheus_javaagent.jar
      - name: JAR_LIST_2
        value: jjwt-jackson-0.11.2.jar:/app/magnolia/WEB-INF/lib
      - name: JAR_LIST_3
        value: jjwt-impl-0.11.2.jar:/app/magnolia/WEB-INF/lib
      - name: JAR_LIST_4
        value: jjwt-api-0.11.2.jar:/app/magnolia/WEB-INF/lib
      - name: JAR_LIST_5
        value: magnolia-rest-security-core-1.0.3.jar:/app/magnolia/WEB-INF/lib
      - name: JAR_LIST_6
        value: magnolia-cloud-bootstrapper-1.0.14.jar:/app/magnolia/WEB-INF/lib
    initScript: /initGo.sh
```

The above `jars` block for the `magnolia-helm` values is equivalent (appart from
the updated jar versions used in the block above) to the following `jars` block
using the "primitve" magnolia-jar images:

```yaml
jars:
  - name: postgres-jdbc
    repository: registry.gitlab.com/mironet/magnolia-jar/postgres-42.2.8
    tag: v0.0.2
    env:
      - name: INIT_DEST
        value: /app/magnolia/WEB-INF/lib
    initScript: /init.sh
  - name: jmx-exporter
    repository: registry.gitlab.com/mironet/magnolia-jar/jmx_prometheus_javaagent-0.13.0
    tag: v0.0.2
    env:
      - name: INIT_DEST
        value: /extraLibs/
    initScript: /init.sh
  - name: jjwt-jackson
    repository: registry.gitlab.com/mironet/magnolia-jar/jjwt-jackson-0.11.2
    tag: v0.0.2
    env:
      - name: INIT_DEST
        value: /app/magnolia/WEB-INF/lib
    initScript: /init.sh
  - name: jjwt-impl
    repository: registry.gitlab.com/mironet/magnolia-jar/jjwt-impl-0.11.2
    tag: v0.0.2
    env:
      - name: INIT_DEST
        value: /app/magnolia/WEB-INF/lib
    initScript: /init.sh
  - name: jjwt-api
    repository: registry.gitlab.com/mironet/magnolia-jar/jjwt-api-0.11.2
    tag: v0.0.2
    env:
      - name: INIT_DEST
        value: /app/magnolia/WEB-INF/lib
    initScript: /init.sh
  - name: magnolia-rest-security-core
    repository: registry.gitlab.com/mironet/magnolia-jar/magnolia-rest-security-core-1.0.3
    tag: v0.0.2
    env:
      - name: INIT_DEST
        value: /app/magnolia/WEB-INF/lib
    initScript: /init.sh
  - name: magnolia-cloud-bootstrapper
    repository: registry.gitlab.com/mironet/magnolia-jar/magnolia-cloud-bootstrapper-1.0.2
    tag: v0.0.2
    env:
      - name: INIT_DEST
        value: /app/magnolia/WEB-INF/lib
    initScript: /init.sh
```

## Advantages of the new image/go-tool

- Only one **image** is required to load all jars. ➡️ Only one **init
  container** is required to load all jars.
- Jars that are loaded are checked for conflicts with jars already present in
  the destination directory.
- There is some basic automatic conflict resolution for situations where there
  already exists a jar with the same artifactID in the destination directory:
  - If a newer (or equal) version of the jar is already present in the
    destination directory, it is **not** loaded.
  - If an older version of the jar is already present in the destination
    directory, the new version is loaded anyways. This default behavior can be
    changed by setting an additional [environment
    variable](#resolve_n-environment-variables).
- If for some reason the version of the already present jar cannot be compared
  to the versions of the jar that is to be loaded, the image returns with an
  error. This default behavior can be changed by setting an additional
  [environment variable](#resolve_n-environment-variables).

## Environment variables

### `JAR_LIST_<n>` environment variables

To load a list of jars into a destination directory, the `JAR_LIST_<n>`
environment variables can be defined. The value of the environment variables is
to be set to:

1. the jar name of one of the jars contained in the image (one of
   [these](artifact_list.json))
2. a colon `:` separator
3. the destination directory where the jar is to be loaded to

(Optional):

4. a colon `:` separator
5. the name that the jar must have in the destination directory

> Note: The program starts looking for an environment varible with `n=0` (i.e.
> `JAR_LIST_0`) and increments `n` until it finds an undefined `JAR_LIST_<n>`
> environment variable.

### `RESOLVE_<n>` environment variables

In cases where the behavior of the automatic conflict resolution can be changed,
this is done by setting the `RESOLVE_<n>` environment variables. The value of
the environment variables is to be set to:

- Either `0` ➡️ **don't load** the respective jar
- Or `1` ➡️ **load** the respective jar

#### Example

The version of the jar defined in `JAR_LIST_3` cannot be compared to the version
of the jar that is already present in the destination directory.

- One can either set environment variable `RESOLVE_3=1` to **load** the jar.
- Or set environment variable `RESOLVE_3=0` to **not load** the jar.

The program won't error out in this case.
