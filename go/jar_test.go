package main

import (
	"bufio"
	"os"
	"reflect"
	"testing"
)

func Test_fetchVersion(t *testing.T) {
	tests := []struct {
		jarName string
		want    version
		wantErr bool
	}{
		{
			jarName: "magnolia-cloud-bootstrapper-1.0.2.jar",
			want: version{
				major:  1,
				minor:  0,
				bugfix: 2,
				suffix: "",
			},
		},
		{
			jarName: "test-test-0.5.1-test-test-java8.jar",
			want: version{
				major:  0,
				minor:  5,
				bugfix: 1,
				suffix: "-test-test-java8",
			},
		},
		{
			jarName: "test-api-2.0.1.Final.jar",
			want: version{
				major:  2,
				minor:  0,
				bugfix: 1,
				suffix: ".Final",
			},
		},
		{
			jarName: "test-v7-compatibility-1.0.jar",
			want: version{
				major:  1,
				minor:  0,
				bugfix: -1,
				suffix: "",
			},
		},
		{
			jarName: "testblabla-1.2-rc8532976.jar",
			want: version{
				major:  1,
				minor:  2,
				bugfix: -1,
				suffix: "-rc8532976",
			},
		},
		{
			jarName: "testblabla-1.01-test-5317-patched.jar",
			want: version{
				major:  1,
				minor:  -1,
				bugfix: -1,
				suffix: ".01-test-5317-patched",
			},
		},
		{
			jarName: "testblabla.jar",
			want: version{
				major:  -1,
				minor:  -1,
				bugfix: -1,
				suffix: "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.jarName, func(t *testing.T) {
			got, err := fetchVersion(tt.jarName)
			if (err != nil) != tt.wantErr {
				t.Errorf("fetchVersion() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("fetchVersion() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_artifactID(t *testing.T) {
	tests := []struct {
		jarName string
		want    string
		wantErr bool
	}{
		{
			jarName: "magnolia-cloud-bootstrapper-1.0.2.jar",
			want:    "magnolia-cloud-bootstrapper",
		},
		{
			jarName: "test-test-0.5.1-test-test-java8.jar",
			want:    "test-test",
		},
		{
			jarName: "test-api-2.0.1.Final.jar",
			want:    "test-api",
		},
		{
			jarName: "test-v7-compatibility-1.0.jar",
			want:    "test-v7-compatibility",
		},
		{
			jarName: "testblabla-1.2-rc8532976.jar",
			want:    "testblabla",
		},
		{
			jarName: "testblabla-1.01-test-5317-patched.jar",
			want:    "testblabla",
		},
		{
			jarName: "testblabla.jar",
			want:    "testblabla",
		},
	}
	for _, tt := range tests {
		t.Run(tt.jarName, func(t *testing.T) {
			got, err := artifactID(tt.jarName)
			if (err != nil) != tt.wantErr {
				t.Errorf("artifactID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("artifactID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_versionID_notErroring(t *testing.T) {
	file, err := os.Open("./test/jarslist.txt")
	if err != nil {
		t.Logf("failed to open file: %s", err)
		return
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		jarName := scanner.Text()
		got, err := fetchVersion(jarName)
		if err != nil {
			t.Fatalf("fetchVersion(%s) error = %v", jarName, err)
		}
		t.Logf("jarName %s -> %v", jarName, got)
	}

	file.Close()
}
