package main

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/urfave/cli/v2"
)

func main() {
	var artifactListPath, jarListPath, jarSource string

	app := &cli.App{
		Name:  "magnolia-jar",
		Usage: "TODO",
	}

	app.Commands = []*cli.Command{
		{
			Name:  "download",
			Usage: "Download a list of maven artifacts.",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "artifact-list-path", Aliases: []string{"l"}, Value: "", Usage: "TODO",
					Destination: &artifactListPath, EnvVars: []string{"ARTIFACT_LIST_PATH"},
				},
			},
			Action: func(ctx *cli.Context) error {
				return download(artifactListPath)
			},
		},
		{
			Name:  "load",
			Usage: "Load a list of jars.",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "jar-list-path", Aliases: []string{"l"}, Value: "", Usage: "A list of jars to load.",
					Destination: &jarListPath, EnvVars: []string{"JAR_LIST_PATH"},
				},
				&cli.StringFlag{
					Name: "jar-source", Aliases: []string{"s"}, Value: "./jars", Usage: "The directory to load the jars from.",
					Destination: &jarSource, EnvVars: []string{"JAR_SOURCE"},
				},
			},
			Action: func(ctx *cli.Context) error {
				return load(jarSource, jarListPath)
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		panic(err)
	}
}

func download(artifactListPath string) error {
	artifactDescriptorList, err := artifactDescriptorList(artifactListPath)
	if err != nil {
		return fmt.Errorf("could not get artifact descriptor list: %w", err)
	}

	for _, artifactDescriptor := range artifactDescriptorList {
		destName := artifactDescriptor.DestName
		if destName == "" {
			destName = strings.Join([]string{artifactDescriptor.ArtifactID, artifactDescriptor.Version}, "-") + ".jar"
		}
		targetPath := path.Join(artifactDescriptor.DestDir, destName)
		err := artifactDescriptor.download(targetPath)
		if err != nil {
			return fmt.Errorf("could not download artifact %s: %w", artifactDescriptor.ArtifactID, err)
		}
		fmt.Printf("successfully downloaded artifact %s to %s\n", artifactDescriptor.ArtifactID, targetPath)
	}

	return nil
}

func load(jarSource, jarListPath string) error {
	jarList, err := jarList(jarListPath)
	if err != nil {
		return fmt.Errorf("could not get jar list: %w", err)
	}

	numLoaded := 0
	for i, jar := range jarList {
		conflicts, err := checkConflict(jar)
		if err != nil {
			return fmt.Errorf("could not check conflicts for jar %s: %w", jar.Name, err)
		}

		mustLoad := false
		switch {
		case len(conflicts) == 0:
			mustLoad = true
		case len(conflicts) == 1:
			switch conflicts[0].resolveAction {
			case dontLoad:
				fmt.Printf("jar %s must not be loaded to destination %s: %s\n", jar.Name, jar.DestDir, conflicts[0].msg)
			case loadAnyways:
				if resolve := os.Getenv(fmt.Sprintf("RESOLVE_%d", i)); resolve == "0" {
					fmt.Printf("%s: jar %s not loaded to destination %s because env var RESOLVE_%d=0\n", conflicts[0].msg, jar.Name, jar.DestDir, i)
				} else {
					fmt.Printf("%s: jar %s loaded to destination %s anyways: to not load it instead specify env var RESOLVE_%d=0\n", conflicts[0].msg, jar.Name, jar.DestDir, i)
					mustLoad = true
				}
			case errorOut:
				resolve := os.Getenv(fmt.Sprintf("RESOLVE_%d", i))
				switch resolve {
				case "0":
					fmt.Printf("%s: resolve conflict for jar %s by not loading to destination %s\n", conflicts[0].msg, jar.Name, jar.DestDir)
				case "1":
					fmt.Printf("%s: resolve conflict for jar %s by loading to destination %s\n", conflicts[0].msg, jar.Name, jar.DestDir)
					mustLoad = true
				default:
					return fmt.Errorf(
						"could not resolve conflict for jar %s: %s: resolve conflict by specifying env var RESOLVE_%d=0 (don't load) or RESOLVE_%d=1 (load)",
						jar.Name, conflicts[0].msg, i, i,
					)
				}

			}
		default:
			resolve := os.Getenv(fmt.Sprintf("RESOLVE_%d", i))
			switch resolve {
			case "0":
				fmt.Printf("unexpected number of conflicts (%d): resolve for jar %s by not loading to destination %s\n", len(conflicts), jar.Name, jar.DestDir)
			case "1":
				fmt.Printf("unexpected number of conflicts (%d): resolve for jar %s by loading to destination %s\n", len(conflicts), jar.Name, jar.DestDir)
				mustLoad = true
			default:
				return fmt.Errorf(
					"unexpected number of conflicts (%d) for jar %s: %v: resolve by specifying env var RESOLVE_%d=0 (don't load) or RESOLVE_%d=1 (load)",
					len(conflicts), jar.Name, conflicts, i, i,
				)
			}
		}

		if mustLoad {
			if err = jar.load(jarSource); err != nil {
				return fmt.Errorf("could not load jar %s: %w", jar.Name, err)
			}
			numLoaded = numLoaded + 1
			name := jar.Name
			if jar.OverwriteName != "" {
				name = jar.OverwriteName
			}
			fmt.Printf("successfully loaded jar %s to %s\n", name, jar.DestDir)
		}
	}

	fmt.Printf("successfully loaded %d jars\n", numLoaded)
	return nil
}

type conflict struct {
	resolveAction resolveAction
	file          os.FileInfo
	msg           string
}

type resolveAction int

const (
	dontLoad resolveAction = iota
	loadAnyways
	errorOut
)

func checkConflict(jar Jar) ([]conflict, error) {
	if jar.DestDir == "" {
		return nil, fmt.Errorf("jar %s has no dest dir", jar.Name)
	}

	wantArtifact, err := artifactID(jar.Name)
	if err != nil {
		return nil, fmt.Errorf("could not get artifact id of jar %s: %w", jar.Name, err)
	}
	wantVersion, err := fetchVersion(jar.Name)
	if err != nil {
		return nil, fmt.Errorf("could not get version of jar %s: %w", jar.Name, err)
	}

	conflicts := make([]conflict, 0)
	if err := filepath.Walk(jar.DestDir, func(path string, info os.FileInfo, walkerr error) error {
		// Skip all subdirectories.
		if info.IsDir() && info.Name() != filepath.Base(jar.DestDir) {
			return filepath.SkipDir
		}
		// Skip all non-jars.
		if filepath.Ext(path) != ".jar" {
			return nil
		}

		haveArtifact, err := artifactID(info.Name())
		if err != nil {
			fmt.Printf("could not get artifact id of jar %s: %v\n", path, err)
			fmt.Printf("continueing with next jar\n")
			return nil
		}
		if haveArtifact != wantArtifact {
			return nil
		}
		haveVersion, err := fetchVersion(info.Name())
		if err != nil {
			fmt.Printf("could not get version of jar %s: %v\n", path, err)
			fmt.Printf("continueing with next jar\n")
			return nil
		}
		haveCompWant, err := haveVersion.compare(wantVersion)
		confl := conflict{
			file: info,
		}
		switch haveCompWant {
		// If the jar we have is the same as the jar we want, we don't need to
		// load the jar.
		case 0:
			confl.resolveAction = dontLoad
			confl.msg = fmt.Sprintf("jar with same artifact id and same version %v already exists", haveVersion)
		// If the jar we have is newer as the jar we want, we don't need to load
		// the jar.
		case 1:
			confl.resolveAction = dontLoad
			confl.msg = fmt.Sprintf("jar with same artifact id and newer version %v already exists", haveVersion)
		// If the jar we have is older than the jar we want, we need to load the
		// jar.
		case -1:
			confl.resolveAction = loadAnyways
			confl.msg = fmt.Sprintf("jar with same artifact id and older version %v already exists", haveVersion)
		// If we can't compare the versions, we need to error out.
		default:
			if err != nil {
				confl.resolveAction = errorOut
				confl.msg = err.Error()
			} else {
				confl.resolveAction = errorOut
				confl.msg = fmt.Sprintf("unexpected compare result %d", haveCompWant)
			}
		}
		conflicts = append(conflicts, confl)
		return nil
	}); err != nil {
		return nil, fmt.Errorf("could not walk through dest dir %s: %w", jar.DestDir, err)
	}
	return conflicts, nil
}
